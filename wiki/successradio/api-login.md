![](logos.png)

**Success Radio API documentation - LOGIN**

[Home](https://bitbucket.org/farfromrefuge/akylas.ipdn.successradio/wiki)

---
The authenicate the APP client and get a TOKEN API
These are the queries that will handle the security to setup the intital API access.

This is part of the API that will collect data related to initial state of the program.
Get a TOKEN to access the API and if successfull GET the 'state' data and return both.

**INITIALLY WE WILL USE AN UNSECURE ---- 'http://'**
**WE ARE IN THE PROCESS OF GETTING AN SSL CERTIFICATE from Rackspace - all the calls will then use ---- 'https://' to mantain security**


### Index
---
[TOC]

---

### LOGIN

---
#### Authenicate
---

**Description** - FUTURE

**Return** - A Authenicate code will be returned

##### Parameters

---
#### Login
---

**Description** - POST the intial user's 'DEVICE' info to the server* and return a TOKEN

This 'info' data will be stored for use in the program. A valid TOKEN will have to be maintained to keep the 'session' open.

If the POST is successful than the 'state' data will be queried and returned as well

**Return** - A TOKEN and the 'state' data will be returned

##### Parameters
----

| parameter | value | description | valid | default |
| --------- | ----- | ----------- | ----- |
| json | array | an json array for user device data | info | info |
| locale | string | The language of the session: return localized data for api calls | **en**,fr | en |
| os | int | The os type | 0 for Apple and 1 for Android | |
| density | string | The device screen density | "medium"/"xhigh"/"xxhigh"... for Apple "high"/"medium"/"xhigh"/... for android| |
| content | string | The content type selected | **01**,02,03,04,05 | 01 = motivate |
| version | string | app version | 1.0.1 | **(optional)** |
| platform | string | platform version | 8.1.x | **(optional)** |


##### Response
---
- 200 -- json
- 400 -- error - OAuth exception
- 401 -- error - Parameter

##### Example request URI(s)
----
```php
curl -X POST -d 'info={\"locale\":\"en\",\"os\":1,\"density\":\"high\",\"content\":\"01\",\"version\":\"8.0\",\"platform\":\"1.0.1\"}' http://successradio.net/oauth2/login.php
```

##### Example Output(s)
---
```json
{
    "access_token": "4c1426cabd8502a1b46f865262e545d19d4a63d8",
    "expires_in": 3600,
    "token_type": "Bearer",
    "scope": null,
    "result": [
        {
            "id": 1,
            "content": "motivate",
            "person": "Speaker",
            "people": "Speakers",
            "img_avatar_default": "http://app.successradio.net/avatar-default/xhdpi/avatar-default.png",
            "url_app_common": "http://app.successradio.net/",
            "url_app_people": "http://app01.successradio.net/",
            "url_app_assets": "http://app011.successradio.net/",
            "url_overview": "http://successradio.net/overview/",
            "url_help": "http://successradio.net/help/",
            "url_terms": "http://successradio.net/terms-of-use/",
            "url_privacy": "http://successradio.net/privacy-notice/",
            "url_ad_aware": "http://successradio.net/ad-aware/"
        }
    ]
}
```json
{
    error: "invalid_client",
    error_description: "The client id supplied is invalid"
}
```
```json
This result is for debug:
{
    "success": true,
    "message": "You have access to Success Radio APIs!"
}
```
##### Notes
---
This call can be found in login.md.

[top](#markdown-header-index)

---
#### renewToken
---

**Description** - *Request for a "token".*

**Return** - *retrieve the elements of the "token" data*

##### Parameters
---
| parameter | value | description | valid | default |
| --------- | ----- | ----------- | ----- |
| CLIENT_ID | string | The client Id (APP ID)| **SRadio created** | sr will provide this |
| end point | url | The uri to send the request | /oauth2/token.php |


##### Response
---
- 200 -- json
- 401 -- error

##### Example request URI(s)
----
```php
curl -X POST -d 'client_id=Pue5rJ3gfs7etzGiu5fQKEYhWEhCz8' http://successradio.net/oauth2/token.php
```

##### Example Output(s)
---
```json
{
    "access_token": "450414aaf2337cc373a54f0231867f702fcaaf88",
    "expires_in": 3600,
    "token_type": "Bearer",
    "scope": null
}
```
```json
{
    error: "invalid_client",
    error_description: "The client id supplied is invalid"
}
```
##### Notes
---
The SRadio APP has been registered at **Success Radio** proper with a **'client_id'** to be used to request an **TOKEN** -- this TOKEN will time out in 3600 seconds and will need to be renewed to continue to access the API.

This call can be found in login.md.

[top](#markdown-header-index)

---
Powered by Akylas - Designed by IP Digital - Copyright (c) Success Radio LLC