![](logos.png)

**Success Radio API documentation - READ FIRST**

[Home](https://bitbucket.org/farfromrefuge/akylas.ipdn.successradio/wiki)

---
**Setup the initial state of the program/session**

This is part of the overall work flost of the API that will use data or effect the functionality related to the **initial or progreesive** state of the program. (Martin let's talk about this part!)

---

1. When there are more than one "content" available, the program will ONLY include THAT "content" and the user will have options:

	- when the user has a premium account,
	- will have the option of selecting the content that they would like
	- the content that was perviously installed on their device will be deleted if not accessed within a set time period. (Martin you and I will determine this)
	- 
2. 

---

### Index
---
[TOC]

---

#### Login

---
#### Authenicate
---

**Description** - We may use in future or maybe for the profile/user "connect" --- not sure just now!*


---
#### POST Device Data
---
**Description** - **POST** the "Device" data back to the server to setup a session for the user. This data is used within the program for ALL subsequent api calls.

```json
curl -X POST -d 'client_id=Pue5rJ3gfs7etzGiu5fQKEYhWEhCz8&info={\"locale\":\"en\",\"os\":0,\"density\":\"xhigh\",\"content\":\"01\",\"version\":\"8.0\",\"platform\":\"1.0.1\"}' http://successradio.net/oauth2/login.php
```

* The server will send back the following reqponse if **POST** was successfull.

##### Example Output(s)
---
```json
{
    "access_token": "450414aaf2337cc373a54f0231867f702fcaaf88",
    "expires_in": 3600,
    "token_type": "Bearer",
    "scope": null
}
```
```json
{
    error: "invalid_client",
    error_description: "The client id supplied is invalid"
}
```
```json
This result is for debug:
{
    "success": true,
    "message": "You have access to Success Radio APIs!"
}
```
##### Notes
---
*Add some exta information here*

[top](#markdown-header-index)

---
#### Renew A Token
---

**Description** - The first step is to call the "OAuth server" to acquire a TOKEN. This **POST** identifies the APP as a registered "client" of SRadio and if successfull will return a TOKEN.

I think if might be a good idea to change up the CLIENT_SECRET every now and again.

```json
curl -X POST -d 'client_id=Pue5rJ3gfs7etzGiu5fQKEYhWEhCz8' http://successradio.net/oauth2/token.php
```

* This TOKEN will expire in 60 minutes, after which a new TOKEN will need to be acquired to access the API. The experation time can adjusted for the best performance and usar experience. - whatever is best for the user and the efficient use of the program

##### Example Output(s)
---
```json
{
    "access_token": "450414aaf2337cc373a54f0231867f702fcaaf88",
    "expires_in": 3600,
    "token_type": "Bearer",
    "scope": null
}
```
```json
{
    error: "invalid_client",
    error_description: "The client id supplied is invalid"
}
```
##### Notes
---
*Add some exta information here*

etc ...

[top](#markdown-header-index)

---
Powered by Akylas - Designed by IP Digital - Copyright (c) Success Radio LLC