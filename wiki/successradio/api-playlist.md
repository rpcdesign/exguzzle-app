![](logos.png)

**Success Radio API documentation - Metadata PLAYLISTS**

[Home](https://bitbucket.org/farfromrefuge/akylas.ipdn.successradio/wiki)

---
The Playlist API
These are the queries that will create random playlists of track metadata and also may include optional insert tracks.

This is part of the API that will collect data related to playlists and tracks of the current content.

### Index
[TOC]

---

### PLAYLISTS

---
#### getPlaylistTestDrive
---

**Description** - *Calls the Metadata Playlist API to get a previously created playlist 'Test Drive'

**Return** - *return a previously created 'Test Drive' playlist.*

##### Parameters
----

| parameter | value | description | valid | default |
| --------- | ----- | ----------- | ----- |
| access_token | hex | The access TOKEN | | |
| request | string | The request type | getPlaylistTestDrive |

##### Response
---
- 200 -- json
- 401 -- error

##### Example request URI(s)
---
```php
curl -X GET "http://successradio.net/api/service.php?access_token=TOKEN&request=getPlaylistTestDrive"
```

##### Example Output(s)
---

```json
{
    "result": [
        {
            "id": 1,
            "person": "01-9999",
            "filename": "http://app01.successradio.net/01-9999/albums/01-999901/tracks/01-999901-17.mp3",
            "duration": 124261,
            "artist": "Philip G",
            "title": "In Studio",
            "album": "Success Radio - Philip G",
            "catalog": "SR01-999901-17",
            "img_profile": "http://app01.successradio.net/01-9999/01/xhdpi-profile.jpg",
            "img_poster": "http://app01.successradio.net/01-9999/albums/01-999901/01/xhdpi-poster.jpg"
        },

...

        {
            "id": 6,
            "person": "01-0006",
            "filename": "http://app01.successradio.net/01-0006/albums/01-000601/tracks/01-000601-05.mp3",
            "duration": 180072,
            "artist": "Bob Proctor",
            "title": "Science Of becoming Wealthy",
            "album": "Success Radio - Bob Proctor",
            "catalog": "SR01-000601-05",
            "img_profile": "http://app01.successradio.net/01-0006/01/xhdpi-profile.jpg",
            "img_poster": "http://app01.successradio.net/01-0006/albums/01-000601/01/xhdpi-poster.jpg"
        },
        {
            "tracks": 6,
            "timemil": 886869,
            "gmdate": "00:14:46"
        }
    ]
}
```

```json
{
    "error": "invalid_token",
    "error_description": "The access TOKEN provided has expired"
}
```
```json
{
    "error": {
        "type": "request",
        "message": "The ' access_token=TOKEN&request=getPlaylistTestDrive ' you provided is not valid or missing!"
    }
}
```
```json
{
    "error": {
        "type": "param",
        "message": "requestuired: a proper 'key' value was not included in the request"
    }
}
```
```json
{
    "error": {
        "type": "param",
        "message": "requestuired: a proper 'insert' value was not included in the request"
    }
}
```
##### Notes
---
** Important to note** is that the images 'img_' have the 'density' *wild char* -- "*****" -- /xhdpi-poster.jpg" - replaced with the proper 'device' density.


This function can be found in playlist.md

[top](#markdown-header-index)

---
#### createPlaylistTestDrive
---

**Description** - *Calls the Playlist API to create a playlist 'Test Drive' and get the tracks that are active in the program,optionally include inserts as well*

**Return** - *create a random playlist and retrieve the elements of ALL Tracks that are "Active" and includes optional inserts in the request*

##### Parameters
----

| parameter | value | description | valid | default |
| --------- | ----- | ----------- | ----- |
| access_token | hex | The access TOKEN | | |
| request | string | The request type | createPlaylistTestDrive |
| key | int | The track count to include in the playlist | # ** *optional**| 20 |
| insert | int | The insert count to include in the playlist - (can be 0) | #  ** *optional**| 5 |
| strip | bool | The param 'strip'can be used to trim the metadata of non essential elements | **true** / false ** *optional** | true |

##### Response
---
- 200 -- json
- 401 -- error

##### Example request URI(s)
---
```php
curl -X GET "http://successradio.net/api/service.php?access_token=TOKEN&request=createPlaylistTestDrive"
```
```php
curl -X GET "http://successradio.net/api/service.php?access_token=TOKEN&request=getPlaylistTestDrive&key=3&insert=1"
```
```php
curl -X GET "http://successradio.net/api/service.php?access_token=TOKEN&request=getPlaylistTestDrive&key=3&insert=1&strip=false"
```

##### Example Output(s)
---
```json
	The Playlist is actually created in the background once a week or so and doesn't return anything but a message

	Success: "Playlist Test Drive was created."

```

---


```json
{
    "error": "invalid_token",
    "error_description": "The access TOKEN provided has expired"
}
```
```json
{
    "error": {
        "type": "request",
        "message": "The ' access_token=TOKEN&request=getPlaylistTestDrive ' you provided is not valid or missing!"
    }
}
```
```json
{
    "error": {
        "type": "param",
        "message": "requestuired: a proper 'key' value was not included in the request"
    }
}
```
```json
{
    "error": {
        "type": "param",
        "message": "requestuired: a proper 'insert' value was not included in the request"
    }
}
```language
```
##### Notes
---
** Important to note** is that the images 'img_' contain the 'density' *wild char* -- "*****" -- /*-poster.jpg" - The star will be replaced with the correct density.

** *The param 'strip'can be used to trim the metadata of non essential elements - i.e. for when the player is active
where you don't need all the other data - - -* **

This function can be found in playlist.md

[top](#markdown-header-index)

---
Powered by Akylas - Designed by IP Digital - Copyright (c) Success Radio LLC