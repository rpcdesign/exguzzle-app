<?php

///namespace EGA\example_guzzle_app\ExGuzzleItemTable;

class ExGuzzleItemTable {
  protected $form;
  protected $data;
  protected $rows;

  public function __construct ($form) {
    ///$this->$form = $form;
  }

  public function createItemTable($form, $data) {

    /// Build the sortable table header.
    $header = array(
      'egaid' => t('Item ID'),
      'name' => t('Name'),
      'username' => t('User Name'),
      'email' => t('Email'),
      'phone' => t('Phone'),
      'website' => t('Website'),
      'status' => t('Status'),
    );

    ///$rows = $data;
    $form['exguzzle_app_table'] = array (
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $this->rows,
      '#empty' => t('No Items found.'),
      '#attributes' => array('class' => array('items')),
    );

    /// EXTRA:  add a pager ui
    $form['pager'] = array('#markup' => theme('pager'));
    /// EXTRA:  return the form to Drupal
    return $form;
  }

  public function getData($table) {
    /// get the data from the db
    $sql = db_select($table, 'ega')->extend('PagerDefault');
    $sql
       ->fields('ega', array('egaid','name','username','email','phone','website'))
       ->orderBy('name', 'ASC')
       ->limit(25);
    $results = $sql->execute();

    $rows = array();
    foreach($results as $result) {
      $rows[] = array(
        'egaid' => check_plain($result->egaid),
        'name' => check_plain($result->name),
        'username' => check_plain($result->username),
        'email' => check_plain($result->email),
        'phone' => check_plain($result->phone),
        'website' => check_url($result->website),
        'status' => l('View', RPCD_ADMIN_PATH . '/exguzzle_app_table_view'),
      );
    }
    $this->rows = $rows;
  }

  public function insertData($table, $data) {
    /// put the lot into the database ...
    foreach ($data as $value) {
      $id = db_insert($table)
      ->fields(array(
        'egaid' => check_plain($value['id']),
        'name' => check_plain($value['name']),
        'username' => check_plain($value['username']),
        'email' => check_plain($value['email']),
        'phone' => check_plain($value['phone']),
        'website' => check_url($value['website']),
      ))
      ->execute();
    }
    watchdog($table, t("!name's info, id !node was !status",
    array('!name' => $value['name'], '!node' => $value['id'],'!status' => 'inserted.')));
    ////exguzzle_app_admin($action, $values);
  }
}
