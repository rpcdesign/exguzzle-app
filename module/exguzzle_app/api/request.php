<?php
///use \exguzzle_app\api\ExampleGuzzleApp;

define('API_PATH', realpath(__DIR__));
///define('API_PATH', realpath('.'));
///echo API_PATH . "<br />";

$paths = array(
  API_PATH,
  API_PATH . '/com',
  get_include_path()
);
set_include_path(implode(PATH_SEPARATOR, $paths));

function __autoload($className) {
  ///$filename = str_replace('\\', '/', $className) . '.php';
  ///require_once $filename;
}
require_once('ExampleGuzzleApp.php');

function request($url) {
  $ega = new ExampleGuzzleApp();
  ///echo $ega->GetRequest($url);
  return $ega->GetRequest($url);
}

///$url = 'https://jsonplaceholder.typicode.com/users/1';
///request ($url);

///echo $ega->GetRequest();
