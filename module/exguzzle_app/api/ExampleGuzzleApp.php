<?php
use Drupal\node\Entity\Node;

///namespace example_guzzle_app\EGA;
///use Drupal\Guzzle\Http\Client;

///echo "Hello Guzzle! AGAIN" . "<br />";

  ///echo "Hello Guzzle!   --  " . __DIR__ . "   ";
/*
  class ExampleGuzzleApp {
    function getName() {
      return "NAME";
    }
  }
*/
///namespace __DIR__ . '\com';

require_once __DIR__ . '/vendor/autoload.php';

/// namespace com;

class ExampleGuzzleApp {
  protected $collection;
  private $status;

  public function __construct () {
  }
  public function GetRequest($url) {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', $url);
    return $this->collection = (string) $res->getBody();
  }

  public function createNode($item) {

    ///$node = (object) NULL;
    $node = new stdClass();
    $node->type = 'eg_app';
    node_object_prepare($node);

    $node->title = 'ExGuzzle App';
    $node->body = 'BODY WARS';
    $node->field_id = $item['id'];
    $node->field_name = $item['name'];
    $node->field_username = $item['username'];
    $node->field_email = $item['email'];
    $node->field_phone = $item['phone'];
    $node->field_website = $item['website'];
    $node->language = 'und';

    $path = 'content/eg_app_' . $item['id'];
    $node->path = array('alias' => $path);

    node_save($node);
    /* Drupal 8
    $node = Node::create([
      'type' => 'example_guzzle_app',
      'id' => $item['id'],
      'name' => $item['name'],
      'username' => $item['username'],
      'email' => $item['email'],
      'phone' => $item['phone'],
      'website' => $item['website']
    ]);
    node_save($node);
    */
  }

}
