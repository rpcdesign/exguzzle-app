
## ExGuzzle App ##



This is a very simple module sample
It is not production ready and I would definitely do things different
The goal was to make a small foot print

It could make better use of views, content types, angularjs, better hooking … etc.
It would depend on best practices and requirements ….
Structure - getting ready for Drupal 8 — Angular2 a better choice as well


#### Installation ####
------------
1) Copy the exguzzle_app directory to the modules folder in your installation.


2) Enable the module using Administer -> Modules (/admin/modules)


#### Default database ####
-------------------
A default db table is provided to get you started organizing your guzzles. You
can override or use it as a template to display api JSON responses.

	exguzzle_app.sql

 — This is not required. You can start from and empty table and ExGuzzle will create one for you.


#### Endpoint testing ####
-------------------
Go to this site to get more info about what you can guzzle.

	Fake Online REST API for Testing and Prototyping
	https://jsonplaceholder.typicode.com/

This endpoint api can be used for testing purposes, but any valid endpoint should work

	Get all the records in this JSON object
	https://jsonplaceholder.typicode.com/users/
	
	Get one(1) single JSON object - the number specifies which one
	https://jsonplaceholder.typicode.com/users/{1 - n}


#### Cases: ####
-------------------
Input Validate
	url — error if not formatted
	endpoint - error if unreachable


This module will install a menu in 
admin/structure/ExGuzzle_app
	use this to guzzle endpoints

admin/structure/ExGuzzle_app_table_view
	use item link to view them as well

#### Recommended Modules ####
-------------------
	guzzle
	cors
	composer


#### Optional Installation ####
---------------------


#### Configuration ####
-------------


#### Support ####
-------
