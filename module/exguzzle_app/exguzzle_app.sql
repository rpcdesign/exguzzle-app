-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 10, 2017 at 05:25 AM
-- Server version: 5.7.17
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drupal_play_01`
--

-- --------------------------------------------------------

--
-- Table structure for table `exguzzle_app`
--

CREATE TABLE `exguzzle_app` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'The primary item ID',
  `egaid` int(10) UNSIGNED NOT NULL COMMENT 'The item ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'Proper name',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT 'User name',
  `email` varchar(255) DEFAULT '' COMMENT 'Email',
  `phone` varchar(255) DEFAULT '' COMMENT 'Phone',
  `website` varchar(2048) DEFAULT '' COMMENT 'Website'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Entities created from remote api call using Guzzle.';

--
-- Dumping data for table `exguzzle_app`
--

INSERT INTO `exguzzle_app` (`id`, `egaid`, `name`, `username`, `email`, `phone`, `website`) VALUES
(2, 1, 'Leanne Graham', 'Bret', 'Sincere@april.biz', '1-770-736-8031 x56442', 'hildegard.org'),
(3, 2, 'Ervin Howell', 'Antonette', 'Shanna@melissa.tv', '010-692-6593 x09125', 'anastasia.net'),
(4, 3, 'Clementine Bauch', 'Samantha', 'Nathan@yesenia.net', '1-463-123-4447', 'ramiro.info'),
(5, 4, 'Patricia Lebsack', 'Karianne', 'Julianne.OConner@kory.org', '493-170-9623 x156', 'kale.biz'),
(6, 5, 'Chelsey Dietrich', 'Kamren', 'Lucio_Hettinger@annie.ca', '(254)954-1289', 'demarco.info'),
(7, 6, 'Mrs. Dennis Schulist', 'Leopoldo_Corkery', 'Karley_Dach@jasper.info', '1-477-935-8478 x6430', 'ola.org'),
(8, 7, 'Kurtis Weissnat', 'Elwyn.Skiles', 'Telly.Hoeger@billy.biz', '210.067.6132', 'elvis.io'),
(9, 8, 'Nicholas Runolfsdottir V', 'Maxime_Nienow', 'Sherwood@rosamond.me', '586.493.6943 x140', 'jacynthe.com'),
(10, 9, 'Glenna Reichert', 'Delphine', 'Chaim_McDermott@dana.io', '(775)976-6794 x41206', 'conrad.com'),
(11, 10, 'Clementina DuBuque', 'Moriah.Stanton', 'Rey.Padberg@karina.biz', '024-648-3804', 'ambrose.net');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exguzzle_app`
--
ALTER TABLE `exguzzle_app`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exguzzle_app`
--
ALTER TABLE `exguzzle_app`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The primary item ID', AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
