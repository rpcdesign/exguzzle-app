<?php

/**
 * Implements hook_form_FORM_ID_alter().
 */
function exguzzle_app_form_FORM_ID_alter($form, $form_state) {

  $sql = db_select('exguzzle_app', 'ega')->extend('PagerDefault');
  $sql
     ->fields('ega', array('egaid','name','username','email','phone','website'))
     ->orderBy('name', 'ASC')
     ->limit(25);
  $results = $sql->execute();

  foreach($results as $result) {
    $rows[] = array(
      check_plain($result->egaid),
      check_plain($result->name),
      check_plain($result->username),
      check_plain($result->email),
      check_plain($result->phone),
      check_url($result->website),
    );
  }
  dsm($rows);

  // Add a checkbox to registration form about agreeing to terms of use.
  $form['exguzzle_app_table'] = array(
    '#options' => $rows,
  );
  return $form;
}
